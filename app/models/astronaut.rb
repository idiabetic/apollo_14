class Astronaut < ApplicationRecord
  validates_presence_of :name, :age, :job
  has_many :astro_missions
  has_many :space_missions, through: :astro_missions
end
