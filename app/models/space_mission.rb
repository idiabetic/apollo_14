class SpaceMission < ApplicationRecord
  has_many :astro_missions
  has_many :astronauts, through: :astro_missions
end
