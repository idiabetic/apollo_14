describe 'user_sees_all_astros' do
  it 'should show multiple astros' do
    astro_1 = Astronaut.create!( name: "Paul", age: 20, job: "Astrosurfing")

    astro_2 = Astronaut.create!( name: "Steve", age: 30, job: "Astrosurfing")
    astro_1.space_missions.create!( title: 'mars', trip_length: 10)
    astro_1.space_missions.create!( title: 'The sun', trip_length: 10000)
    astro_2.space_missions.create!( title: 'jupyter', trip_length: 100)

    visit '/astronauts'

    expect(page).to have_content(astro_1.name)
    expect(page).to have_content(astro_1.age)
    expect(page).to have_content(astro_1.job)


    expect(page).to have_content(astro_2.name)
    expect(page).to have_content(astro_2.age)
    expect(page).to have_content(astro_2.job)

    expect(page).to have_content('Average Age: 25')


    expect(page).to have_content(astro_1.space_missions.first.title)
    expect(page).to have_content(astro_1.space_missions.sum(:trip_length))
  end
end
